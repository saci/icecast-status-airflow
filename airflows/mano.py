import json
from datetime import datetime, timedelta
import pytz

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.operators.bash_operator import BashOperator

#dt.replace(tzinfo=pytz.utc).astimezone(pytz.timezone('America/Recife'))

def iceTeste():

    
    ts = datetime.now().replace(tzinfo=pytz.utc).astimezone(pytz.timezone('America/Recife'))
    
    with open ('/usr/local/airflow/files/icecast/status-json.xsl') as file:
        orelha = json.load(file)
    
    sources = orelha['icestats']['source']
    
    #conferir em que indice está Aconchego
    x = open ('/usr/local/airflow/files/ouvintes.txt', 'a')
    for xx in sources:
        x.write(str(ts))
        for y in ['listeners', 'listenurl','server_name', 'server_url', 'title']:
            try:
                if (y != 'listeners'):
                    texto = '"' + str(xx[y]) + '"'
                else:
                    texto = str(xx[y])
            except:
                texto = '-'
            x.write(',' + texto)
        x.write ('\n')
    x.close()
    

    
       
dag = DAG('Mano', description='Simple listeners scrap from Icecast',
           schedule_interval=timedelta(minutes=3),
           start_date=datetime(2020, 5, 6), catchup=False)


operador_mano  = PythonOperator(task_id='manin', python_callable=iceTeste, dag=dag)

operador_curl = BashOperator(task_id='Curl', \
                             bash_command='curl https://orelha.radiolivre.org/status-json.xsl > /usr/local/airflow/files/icecast/status-json.xsl',\
                             retries=3, dag=dag)

operador_rm = BashOperator(task_id='Remove', \
                             bash_command='rm /usr/local/airflow/files/icecast/status-json.xsl',\
                             retries=3, dag=dag)


operador_curl >> operador_mano >> operador_rm
